<?php


class Disc extends Product
{
    public $size;

    public function __construct(array $args = [])
    {
        $this->table_name = 'discs';
        $this->schema = [
            'sku',
            'name',
            'price',
            'size'
        ];

        $this->sku = $args["sku"] ?? null;
        $this->name = $args["name"] ?? null;
        $this->price = $args["price"] ?? null;
        $this->size = $args["size"] ?? null;
    }
}