<?php


class Furniture extends Product
{
    public $dimension;

    public function __construct(array $args = [])
    {
        $this->table_name = 'furniture';
        $this->schema = [
            'sku',
            'name',
            'price',
            'dimension'
        ];

        $this->sku = $args["sku"] ?? null;
        $this->name = $args["name"] ?? null;
        $this->price = $args["price"] ?? null;
        $this->dimension = $args["dimension"] ?? null;
    }
}