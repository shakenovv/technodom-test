<?php require_once('../private/initialize.php'); ?>

<?php

$disc_products = (new Disc())->getAll();
$book_products = (new Book())->getAll();
$furniture_products = (new Furniture())->getAll();

$products = array_merge($disc_products, $book_products, $furniture_products);

?>

<!doctype html>

<html lang="en">
    <head>
        <title>Technodom Test</title>
        <meta charset="utf-8">
        <link rel="stylesheet" media="all" href="<?php echo url_for('/stylesheets/public.css'); ?>" />
    </head>

    <body>
        <header>
            <h1>
                <a href="<?php echo url_for('/index.php'); ?>">Product List</a>
            </h1>
            <h2>
                <a href="<?php echo url_for('/create.php'); ?>">Add product</a>
            </h2>
        </header>

        <div id="main">
          <ul id="menu">
            <?php
                foreach ($products as $product) {
                    echo "
                        <li>
                            <a>" . $product["sku"] . "</a>
                            <a>" . $product["name"] . "</a>
                            <a>$" . $product["price"] . "</a>
                            " . (isset($product["size"]) ? sprintf("<a>Size: %s GB</a>", $product["size"]) : null) . "
                            " . (isset($product["weight"]) ? sprintf("<a>Weight: %s g</a>", $product["weight"]) : null) . "
                            " . (isset($product["dimension"]) ? sprintf("<a>Dimensions: %s</a>", $product["dimension"]) : null) . "
                        </li>
                    ";
                }
            ?>
          </ul>
        </div>
    </body>
</html>
