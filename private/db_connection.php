<?php

function OpenCon() {
    $conn = null;

    try {
        $conn = new PDO("mysql:host=database;port=3306;dbname=technodom-test", "root", "root");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }

    return $conn;
}
