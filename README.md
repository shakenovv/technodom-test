#Technodom Test

- Simple run ```docker-compose up -d ``` to run application
- If you don't use docker, index.php file is in public folder
- You should import database scripts, which are in /private/db_dump

If you use docker:
- mysql database server is in port: 33061
- nginx server in port: 8080
- you can change properties in "docker-compose.yml" file and in /docker folder files