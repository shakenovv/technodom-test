<?php


class Product
{
    public $sku;
    public $name;
    public $price;

    protected $table_name;
    protected $schema;

    public function getSchema() {
        return $this->schema;
    }

    public function getTableName() {
        return $this->table_name;
    }

    public function save() : void {
        $conn = OpenCon();
        $query = null;

        //get product by id(sku)
        $sth = $conn->prepare('SELECT * FROM ' . $this->table_name . ' WHERE sku = ' . $this->sku);
        $sth->execute();
        $obj = $sth->fetch(PDO::FETCH_ASSOC);

        //if item with this sku does not exist (create it)
        if ($obj == false) {
            $fields = "";
            $placeholders = "";

            foreach ($this->schema as $field) {
                $lastChar = end($this->schema) != $field ? ", " : "";
                $thisField = gettype($this->$field) == "string" ? ("'" . $this->$field . "'") : ($this->$field);

                $fields = $fields . $field . $lastChar;
                $placeholders = $placeholders . $thisField . $lastChar;
            }

            $query = sprintf(
                "INSERT INTO `%s` (%s) VALUES (%s)",
                $this->table_name,
                $fields,
                $placeholders
            );
        }
        // else update it
        else {
            $fields = "";

            foreach ($this->schema as $field) {
                $lastChar = end($this->schema) != $field ? ", " : "";
                $thisField = gettype($this->$field) == "string" ? ("'" . $this->$field . "'") : ($this->$field);

                $fields = $fields . $field . " = " . $thisField . $lastChar;
            }

            $query = sprintf(
                "UPDATE `%s` SET %s WHERE sku = %s",
                $this->table_name,
                $fields,
                $this->sku
            );
        }

        try {
            $conn->exec($query);
        } catch (PDOException $exception) {
            echo "Error while executing query: " . $exception->getMessage();
        }

        $conn = null;
    }

    public function getAll() : array {
        $conn = OpenCon();

        $sth = $conn->query("SELECT * FROM " . $this->table_name);
        $sth->execute();

        $items = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $items;
    }
}
