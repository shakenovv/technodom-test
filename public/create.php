<?php require_once('../private/initialize.php'); ?>

<!doctype html>

<?php

require_once('../private/initialize.php');

$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $is_valid = true;

    //validate every column as required
    foreach ((new $_POST["class_type"]())->getSchema() as $column) {
        if (empty($_POST[$column])) {
            $errors[$column] = "$column is required";
            $is_valid = false;
        }
    }

    if ($is_valid) {
        $obj = new $_POST["class_type"]($_POST);
        $obj->save();

        redirect_to("index.php");
    }
}

?>

<html lang="en">
<head>
    <title>Technodom Test</title>
    <meta charset="utf-8">
    <link rel="stylesheet" media="all" href="<?php echo url_for('/stylesheets/public.css'); ?>" />
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function() {
            $("select").on('change', function (e) {
                $(".class_block").hide();
                $("#" + $(this).val()).show();
            });
            
            function init(id) {
                $(".class_block").hide();
                $("#" + $("select").val()).show();
            }
            init();
        });
    </script>
</head>

<body>
<header>
    <h1>
        <a href="<?php echo url_for('/index.php'); ?>">Product List</a>
    </h1>
    <h2>
        <a href="<?php echo url_for('/create.php'); ?>">Add product</a>
    </h2>
</header>

<div id="main">
    <form action="create.php" method="POST">
        <div style="margin-top: 10px">
            <label for="sku">SKU: </label>
            <input type="text" name="sku" />
            <span class="error">* <?php echo $errors["sku"] ?? null;?></span>
        </div>
        <div style="margin-top: 10px">
            <label for="name">Name: </label>
            <input type="text" name="name" />
            <span class="error">* <?php echo $errors["name"] ?? null;?></span>
        </div>
        <div style="margin-top: 10px">
            <label for="price">Price: </label>
            <input type="text" name="price" />
            <span class="error">* <?php echo $errors["price"] ?? null;?></span>
        </div>

        <div style="margin-top: 10px">
            <label for="class_type">Type switcher: </label>
            <select name="class_type">
                <option value="Disc" selected>Disc</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
            </select>
        </div>

        <div>
            <div id="Disc" style="display:none;" class="class_block">
                <div style="margin-top: 10px">
                    <label for="size">Size: </label>
                    <input type="text" name="size" />
                    <span class="error">* <?php echo $errors["size"] ?? null;?></span>
                    <p>Size should be filled in GB (gigabytes), must be float.</p>
                </div>
            </div>
            <div id="Book" style="display:none;" class="class_block">
                <div style="margin-top: 10px">
                    <label for="weight">Weight: </label>
                    <input type="text" name="weight" />
                    <span class="error">* <?php echo $errors["weight"] ?? null;?></span>
                    <p>Weight should be filled in grams, must be float.</p>
                </div>
            </div>
            <div id="Furniture" style="display:none;" class="class_block">
                <div style="margin-top: 10px">
                    <label for="dimension">Dimension: </label>
                    <input type="text" name="dimension" />
                    <span class="error">* <?php echo $errors["dimension"] ?? null;?></span>
                    <p>Please provide dimensions in Height x Width x Length (HxWxL) format.</p>
                </div>
            </div>
        </div>

        <div style="margin-top: 10px"><input type="submit" name="submit" value="Save" /></div>
    </form>
</div>
</body>
</html>
