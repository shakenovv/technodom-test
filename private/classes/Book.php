<?php


class Book extends Product
{
    public $weight;

    public function __construct(array $args = [])
    {
        $this->table_name = 'books';
        $this->schema = [
            'sku',
            'name',
            'price',
            'weight'
        ];

        $this->sku = $args["sku"] ?? null;
        $this->name = $args["name"] ?? null;
        $this->price = $args["price"] ?? null;
        $this->weight = $args["weight"] ?? null;
    }
}